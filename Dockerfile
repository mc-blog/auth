FROM node:9.11.1-alpine

WORKDIR /app

ADD . /app

COPY package*.json ./

RUN ["npm", "install"]

RUN ["npm", "run", "compile"]

EXPOSE 3000

CMD ["/src/build/bin/boot.js"]
