import * as express from 'express';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as nconf from 'nconf';
import * as passport from 'passport';
import * as winston from 'winston';
import Routes from './routes/Index';
import UserRepository from './database/UserRepository';
import PassportOauthGoogle from './strategies/PassportOauthGoogle';

export default class App {

    private readonly port: number;
    private readonly server: any;
    private routes: any;
    private userRepository: UserRepository;
    private passportOauthGoogle: any;

    constructor() {
        this.port = nconf.get('api:port') || 3000;
        this.server = express();
        this.userRepository = new UserRepository();
        this.routes = new Routes(this.server);
        this.passportOauthGoogle = new PassportOauthGoogle(this.userRepository);
    }

    public async run() {
        winston.info('Starting Application...');
        const runDate = _.now();

        await this.connectMongo();
        await this.runServer();

        winston.info(`Application started in ${_.now() - runDate}ms`);
    }

    private connectMongo() {
        winston.info('Connecting with Mongo');
        return mongoose.connect('mongodb://mongo:27017/blog')
            .then(() => {
                winston.info('Successful connect with Mongo');
            })
            .catch((e) => {
                winston.error('Unable connect with Mongo', JSON.stringify(e));
                process.exit(1);
            });
    }

    private runServer() {
        this.server.use(passport.initialize());

        this.routes.prepare();

        this.passportOauthGoogle.init();

        return this.server
            .listen(this.port, () => {
                winston.info(`Auth api listen on port ${this.port}`);
            });
    }
}
