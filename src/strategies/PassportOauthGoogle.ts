import * as nconf from 'nconf';
import * as passport from 'passport';
import * as winston from 'winston';
import { Strategy as GoogleStrategy } from 'passport-google-oauth2';
import UserRepository from '../database/UserRepository';

export default class PassportOauthGoogle {
    constructor(private userRepository: UserRepository) {}

    public init() {
        passport.serializeUser((user: any, done) => {
            done(null, user.id);
        });

        passport.deserializeUser((id, done) => {
            this.userRepository
                .getById(id)
                .then((user) => {
                    done(null, user);
                });
        });

        const googleConfig = {
            callbackURL: '/auth/google/redirect',
            clientID: nconf.get('google:clientId'),
            clientSecret: nconf.get('google:clientSecret'),
        };

        passport.use(new GoogleStrategy(googleConfig, async (accessToken, refreshToken, profile, done) => {
            let user = null;

            try {
                user = await this.userRepository.getByGoogleId(profile.id);
            } catch (e) {
                winston.error(e);
            }

            if (user) {
                return done(null, user);
            }

            try {
                user = await this.userRepository.create({
                    email: profile.email,
                    googleId: profile.id,
                    username: profile.displayName,
                });
            } catch (e) {
                winston.error(e);
            }

            done(null, user);
        }));
    }
}
