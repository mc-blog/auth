import User from './UserModel';

export default class UserRepository {
    private model: any;

    constructor() {
        this.model = User;
    }

    public getModel() {
        return this.model;
    }

    public create(user) {
        return this.getModel()
            .create(user);
    }

    public getById(id) {
        return this.getModel()
            .findById(id)
            .exec();
    }

    public getByGoogleId(googleId) {
        return this.getModel()
            .findOne({ googleId })
            .exec();
    }
}