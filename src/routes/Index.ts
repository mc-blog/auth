import * as jwt from 'jsonwebtoken';
import * as nconf from 'nconf';
import * as passport from 'passport';

export default class Routes {

    constructor(private server: any) {}

    public prepare() {
        this.server.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email' ]}));

        this.server.get('/auth/google/redirect', passport.authenticate('google'), (req, res) => {
            const { id, username, email, googleId } = req.user;
            const user = { id, username, email, googleId };
            const secret = nconf.get('jwt:secret') || 's@cr@t';
            const token = jwt.sign(user, secret, { expiresIn: '24h' });
            res.redirect(`http://localhost:8000/?token=${token}`);
        });
    }
}
